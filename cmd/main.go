package main

import (
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/navenest/gitea-mirror-helper/internal/gitea"
	"gitlab.com/navenest/gitea-mirror-helper/internal/gitlab"
	"gitlab.com/navenest/golang-library/pkg/logger/log"
	"go.uber.org/zap"
)

type mirror struct {
	SourceApi      string `mapstructure:"source_api"`
	SourceGroup    string `mapstructure:"source_group"`
	DestinationApi string `mapstructure:"destination_api"`
	DestinationOrg string `mapstructure:"destination_org"`
}

func main() {

	viper.SetConfigName("config")   // name of config file (without extension)
	viper.SetConfigType("yaml")     // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath("config/")  // path to look for the config file in
	viper.AddConfigPath("example/") // call multiple times to add many search paths
	viper.AddConfigPath(".")        // optionally look for config in the working directory
	err := viper.ReadInConfig()     // Find and read the config file
	if err != nil {                 // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %w", err))
	}

	var mirrorList []mirror
	err = viper.UnmarshalKey("mirrors", &mirrorList)
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %w", err))
	}
	log.Logger.Info("config",
		zap.Any("mirrors", mirrorList),
		zap.Any("config", viper.ConfigFileUsed()))
	for _, m := range mirrorList {
		log.Logger.Info("source info",
			zap.String("source_api", m.SourceApi),
			zap.String("source_group", m.SourceGroup),
		)
		projects, err := gitlab.GetGitlabProjects(m.SourceGroup, m.SourceApi)
		if err != nil {
			log.Logger.Error("Unable to get projects",
				zap.Error(err))
			return
		}
		log.Logger.Info("Got projects",
			zap.Any("projects", projects))
		giteaRepos, err := gitea.GetGiteaRepos(m.DestinationOrg, m.DestinationApi)
		if err != nil {
			log.Logger.Error("Unable to get repos",
				zap.Error(err))
			return
		}
		log.Logger.Info("Got repos",
			zap.Any("repos", giteaRepos))

		err = gitea.CreateGiteaMirrors(
			m.DestinationOrg,
			m.DestinationApi,
			projects,
			giteaRepos,
		)
		if err != nil {
			log.Logger.Error("Unable to create repos",
				zap.Error(err))
		}
	}

}
