######## Start a new stage from scratch #######
FROM golang:1.22 as golang-builder

WORKDIR /app

# Copy go mod and sum files
COPY go.mod go.sum ./
COPY . .
# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

#RUN go test -json ./...
# RUN CGO_ENABLED=0 go test ./... -v -cover
RUN CGO_ENABLED=0 GOOS=linux go build -a -o main ./cmd/

######## Start a new stage from scratch #######
FROM debian:bullseye-slim AS base-builder
WORKDIR /app/

RUN mkdir -p dist
RUN apt update && apt install -y ca-certificates

FROM gcr.io/distroless/base:nonroot

COPY --from=base-builder /etc/ssl/certs /etc/ssl/certs

USER 65534:65534

WORKDIR /app/
# Copy the Pre-built binary file from the previous stage
COPY --chown=65534:65534 --from=golang-builder /app/main .


# Command to run the executable
CMD ["./main"]

