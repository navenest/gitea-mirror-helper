package gitea

import (
	"bytes"
	"encoding/json"
	"errors"
	"gitlab.com/navenest/gitea-mirror-helper/internal/gitlab"
	"gitlab.com/navenest/golang-library/pkg/logger/log"
	"go.uber.org/zap"
	"io"
	"net/http"
	"os"
)

type Repo struct {
	Name    string `json:"name"`
	HttpUrl string `json:"clone_url"`
	SshUrl  string `json:"ssh_url"`
	Mirror  bool   `json:"mirror"`
}

func GetGiteaRepos(org string, giteaUrl string) ([]Repo, error) {
	giteaToken := os.Getenv("GITEA_TOKEN")

	giteaUrl = giteaUrl + "/orgs/" + org + "/repos"
	req, err := http.NewRequest("GET", giteaUrl, nil)
	if giteaToken != "" {
		auth := "Bearer " + giteaToken
		req.Header.Add("Authorization", auth)
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Logger.Error("Unable to perform request",
			zap.Error(err),
			zap.String("giteaUrl", giteaUrl))
		return []Repo{}, err
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Logger.Error("Unable to close response body",
				zap.Error(err))
			return
		}
	}(resp.Body)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Logger.Error("Unable to read response body",
			zap.Error(err))
	}

	var repos []Repo
	err = json.Unmarshal(body, &repos)

	log.Logger.Info("Got projects",
		zap.Any("projects", repos))
	return repos, nil
}

type mirrorPayload struct {
	CloneAddress   string `json:"clone_addr"`
	Mirror         bool   `json:"mirror"`
	MirrorInterval string `json:"mirror_interval"`
	RepoName       string `json:"repo_name"`
	RepoOwner      string `json:"repo_owner"`
}

func CreateGiteaMirrors(org string, giteaUrl string, gitlabProjects []gitlab.Project, giteaRepos []Repo) error {
	giteaToken := os.Getenv("GITEA_TOKEN")

	giteaUrl = giteaUrl + "/repos/migrate"

	for _, project := range gitlabProjects {
		skipFlag := false
		for _, repo := range giteaRepos {
			if project.Name == repo.Name {
				log.Logger.Info("Repo already exists",
					zap.String("name", repo.Name),
					zap.String("org", org))
				skipFlag = true
				break
			}
		}

		if skipFlag {
			continue
		}

		payload := mirrorPayload{
			CloneAddress:   project.HttpUrl,
			Mirror:         true,
			MirrorInterval: "1h",
			RepoName:       project.Name,
			RepoOwner:      org,
		}

		payloadBytes, err := json.Marshal(payload)
		if err != nil {
			return err
		}

		req, err := http.NewRequest("POST", giteaUrl, bytes.NewBuffer(payloadBytes))
		if err != nil {
			return err
		}

		if giteaToken != "" {
			auth := "Bearer " + giteaToken
			req.Header.Add("Authorization", auth)
		} else {
			log.Logger.Error("Token is required for auth",
				zap.String("giteaUrl", giteaUrl))
			return errors.New("token is required for auth")
		}

		req.Header.Set("Content-Type", "application/json")

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			log.Logger.Error("Unable to perform request",
				zap.Error(err),
				zap.String("giteaUrl", giteaUrl))
			return err
		}
		if resp.StatusCode == 201 {
			log.Logger.Info("Created mirror",
				zap.String("giteaUrl", giteaUrl),
				zap.String("name", project.Name))
		} else {
			bodyBytes, err := io.ReadAll(resp.Body)
			if err != nil {
				log.Logger.Error("Unable to read response body",
					zap.Error(err))
			}
			bodyString := string(bodyBytes)
			log.Logger.Error("Unable to create mirror",
				zap.Int("statusCode", resp.StatusCode),
				zap.String("body", bodyString))
		}

	}

	return nil
}
