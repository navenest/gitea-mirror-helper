package gitlab

import (
	"encoding/json"
	"gitlab.com/navenest/golang-library/pkg/logger/log"
	"go.uber.org/zap"
	"io"
	"net/http"
	"net/url"
	"os"
)

type Project struct {
	Name    string `json:"name"`
	HttpUrl string `json:"http_url_to_repo"`
	SshUrl  string `json:"ssh_url_to_repo"`
}

func GetGitlabProjects(group string, gitlabUrl string) ([]Project, error) {
	gitlabToken := os.Getenv("GITLAB_TOKEN")

	gitlabUrl = gitlabUrl + "/groups/" + url.QueryEscape(group) + "/projects"
	req, err := http.NewRequest("GET", gitlabUrl, nil)
	if gitlabToken != "" {
		auth := "Bearer " + gitlabToken
		req.Header.Add("Authorization", auth)
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Logger.Error("Unable to perform request",
			zap.Error(err),
			zap.String("gitlabUrl", gitlabUrl))
		return []Project{}, err
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Logger.Error("Unable to close response body",
				zap.Error(err))
			return
		}
	}(resp.Body)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Logger.Error("Unable to read response body",
			zap.Error(err))
	}

	var projects []Project
	err = json.Unmarshal(body, &projects)

	log.Logger.Info("Got projects",
		zap.Any("projects", projects))
	return projects, nil
}
